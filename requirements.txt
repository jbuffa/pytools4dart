setuptools==40.2.0
numpy==1.16.1
pandas==0.23.4
lxml==4.2.5
matplotlib
rtree
plyfile
matplotlib
scipy
cython
lmfit
pyjnius
rasterio
tinyobjloader==2.0.0rc5
laspy
git+https://gitlab.com/pytools4dart/generateds.git
git+https://gitlab.com/pytools4dart/gdecomp.git
git+https://github.com/jgomezdans/prosail.git


